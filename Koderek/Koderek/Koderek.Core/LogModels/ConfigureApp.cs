﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using Koderek.Core.Enums;

namespace Koderek.Core.LogModels
{
    public static class ConfigureApp
    {
        public static char[] AllowableInPass = new char[102]
        {
            //' ',    //znak pusty zeby numeracja pierwszego elem tablicy byla 1
            ' ',    //dopuszczamy tutaj rowniez szyfrowanie spacji
            'a','ą','b','c','d','e','ę','f','g','h','i','j','k','l','ł','m','n','o','ó','p','q','r','s','ś','t','u','w','v','x','y','z',
            'A','Ą','B','C','D','E','Ę','F','G','H','I','J','K','L','Ł','M','N','O','Ó','P','Q','R','S','Ś','T','U','W','V','X','Y','Z',
            '0','1','2','3','4','5','6','7','8','9',
            '!','"','#','$','%','&','(',')','*','+','-','.','/',':',';','<','=','>','?','@','[',',',']','^','_','`','{','|','}'
        };
        // brakujące: ' \

        public static string ActiveFrameBackground = "#cff6ff";
        public static string NoActiveFrameBackground = "#dfe6e9";

        public static int CurrentKindOfCode = (int)KindOfCode.NoSelectedKindOfCode;   //aktualny wybór, na starcie brak kodu

        public static int CaesarTranslation = 1;            //przesuniecie w algorytmie Caesar. Na starcie =1
        public static int CaesarMinTranslation = 1;
        public static int CaesarMaxTranslation = 255;
        public static char CaesarFirstChar = '!';           //pierwszy dopuszczalny znak  w kodzie Cezara, kod 33
        public static char CeasatLastChar = '}';            //ostatni  dopuszczalny znak  w kodzie Cezara, kod 125
    }
}
