﻿using System;
using System.Collections.Generic;
using System.Text;
using Android.Icu.Text;
using Android.Widget;
using Koderek.Core.LogModels;

namespace Koderek.Core.IServices
{
    /// <summary>
    /// Szyfr Cezara oparty na wlasnej tablicy znakow
    /// Na wejściu jest ustawione albo InText(do szyfru) albo OutText(Po szyfrowaniu) i Translation(przesunięcie)
    /// </summary>
    class Caesar2:ICoder
    {
        public string InText;       //tekst szyfrowany
        public string OutText;      //tekst deszyfrowany
        
        public void EnCode(object o)
        {

            string w = "";
            int t = ConfigureApp.CaesarTranslation;
            int DimArray = ConfigureApp.AllowableInPass.Length;

            //Tutaj zasadnicza czesc
            //w = "Szyfr Cezara ver 2 - kodowanie";

            char first = ConfigureApp.AllowableInPass[0];
            char last = ConfigureApp.AllowableInPass[DimArray-1];

            t = t % DimArray;

            foreach (var c in InText)
            {
                if (PosOfChar(c) + t > PosOfChar(last))
                {
                    w += ConfigureApp.AllowableInPass[(PosOfChar(c) + t) % PosOfChar(last) + PosOfChar(first) - 1];
                }
                else
                {
                    w += ConfigureApp.AllowableInPass[PosOfChar(c) + t];
                }

            }

            //OutText = "TEST_EnCode_Ceasar";
            OutText = w;
        }

        public void DeCode(object o)
        {

            string w = "";
            int t = ConfigureApp.CaesarTranslation;
            int DimArray = ConfigureApp.AllowableInPass.Length;


            //Tutaj zasadnicza czesc
            //w = "Szyfr Cezara ver 2 - dekodowanie";

            char first = ConfigureApp.AllowableInPass[0];
            char last = ConfigureApp.AllowableInPass[DimArray - 1];

            t = t % DimArray;  

            foreach (var c in OutText)
            {
                if (PosOfChar(c) - t < PosOfChar(first))
                {
                   w += ConfigureApp.AllowableInPass[PosOfChar(last) -  Math.Abs(PosOfChar(c) - t)+1];
                }
                else
                {
                    w += ConfigureApp.AllowableInPass[PosOfChar(c) - t];
                }

            }

            //InText = "TEST_DeCode_Ceasar";
            InText = w;
        }

        private int PosOfChar(char z)
        {
            int result = 0;    

            foreach (var item in ConfigureApp.AllowableInPass)
            {
                if (item == z)
                    break;
                else
                    result++;
            }

            return result;
        }

    }
}
