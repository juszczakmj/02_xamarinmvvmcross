﻿using System;
using System.Collections.Generic;
using System.Text;
using Koderek.Core.LogModels;

namespace Koderek.Core.IServices
{
    /// <summary>
    /// Szyfr Cezara oparty na tablicy kodow ASCII
    /// Na wejściu jest ustawione albo InText(do szyfru) albo OutText(Po szyfrowaniu) i Translation(przesunięcie)
    /// </summary>
    class Caesar:ICoder
    {
        public string InText;       //tekst szyfrowany
        public string OutText;      //tekst deszyfrowany

        public void EnCode(object o)
        {

            string w = "";
            int t = ConfigureApp.CaesarTranslation;
            
            char first = ConfigureApp.CaesarFirstChar;
            char last = ConfigureApp.CeasatLastChar;

            int DimArray = (int)last - (int)first + 1;

            t = t % DimArray;

            foreach (var c in InText)
            {
                if ((int) (c) + t > (int) last)
                {
                    w += (char)(((int)(c) + t) % (int)last + (int)first - 1);
                }
                else
                {
                    w += (char)((int) (c) + t);
                }
               
            }

            //OutText = "TEST_EnCode_Ceasar";
            OutText = w;
        }

        public void DeCode(object o)
        {

            string w = "";
            int t = ConfigureApp.CaesarTranslation;
           
            char first = ConfigureApp.CaesarFirstChar;
            char last = ConfigureApp.CeasatLastChar;

            int DimArray = (int)last - (int)first + 1;

            t = t % DimArray;

            foreach (var c in OutText)
            {
                if ((int)(c) - t < (int)first)
                {
                    w += (char)((int)last - ((int)first - (((int)(c) - t) % (int)first)) + 1);
                }
                else
                {
                    w += (char)((int)(c) - t);
                }

            }

            //InText = "TEST_DeCode_Ceasar";
            InText = w;
        }
    }
}
