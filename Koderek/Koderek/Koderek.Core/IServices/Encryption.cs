﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Koderek.Core.IServices
{
    class Encryption
    {
        /// <summary>
        /// Zwraca void poniewaz obiekt o bedzie mial pole InText i OutText i funkcja je ustawi
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="o"></param>
        public void EnCode<T>(T o) where T : ICoder
        {
            o.EnCode(o);
        }

        public void DeCode<T>(T o) where T : ICoder
        {
            o.DeCode(o);
        }

    }
}
