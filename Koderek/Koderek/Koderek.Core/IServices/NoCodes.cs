﻿using System;
using System.Collections.Generic;
using System.Text;
using Koderek.Core.LogModels;

namespace Koderek.Core.IServices
{
    /// <summary>
    /// NIE WYBRANA ŻADNEGO KODU
    /// Na wejściu jest ustawione albo InText(do szyfru) albo OutText(Po szyfrowaniu) i Translation(przesunięcie)
    /// </summary>
    class NoCodes : ICoder
    {
        public string InText;       //tekst szyfrowany
        public string OutText;      //tekst deszyfrowany

        public void EnCode(object o)
        {

            string w = "";
            int t = ConfigureApp.CaesarTranslation;

            w = "Wybierz rodzaj szyfru...";

            OutText = w;
        }

        public void DeCode(object o)
        {

            string w = "";
            int t = ConfigureApp.CaesarTranslation;

            w = "Wybierz rodzaj szyfru...";

            InText = w;
        }
    }
}
