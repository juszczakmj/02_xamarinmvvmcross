﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Koderek.Core.IServices
{
    interface ICoder
    {
        void EnCode(object o);    //zakodowanie
        void DeCode(object o);    //dekodowanie       
    }
}
