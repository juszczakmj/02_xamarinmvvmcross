﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Koderek.Core.Pages
{
    public partial class FirstPage
    {
        public FirstPage()
        {
            InitializeComponent();
        }

        async void OnChangeTapped(object sender, EventArgs e)
        {
            await Task.WhenAny<bool>
            (
                ChangeImage.ScaleTo(0.25, 125)
            );
            await ChangeImage.ScaleTo(1, 125);
        }

        async void OnEyeTapped(object sender, EventArgs e)
        {
            await Task.WhenAny<bool>
            (
                EyeImage.ScaleTo(0.25, 125)
            );
            await EyeImage.ScaleTo(1, 125);
        }

        async void OnShareTapped(object sender, EventArgs e)
        {
            await Task.WhenAny<bool>
            (
                ShareImage.ScaleTo(0.25, 125)

            );
            await ShareImage.ScaleTo(1, 125);
        }

        async void OnCopyToClipboardTapped(object sender, EventArgs e)
        {
            await Task.WhenAny<bool>
            (
                CopyImage.ScaleTo(0.25, 125)
            );
            await CopyImage.ScaleTo(1, 125);
        }

        async void OnCopyFromTapped(object sender, EventArgs e)
        {
            await Task.WhenAny<bool>
            (
                CopyFromImage.ScaleTo(0.25, 125)
            );
            await CopyFromImage.ScaleTo(1, 125);
        }

        async void OnRefreshTapped(object sender, EventArgs e)
        {
            await Task.WhenAny<bool>(
                RefreshImage.ScaleTo(0.25, 125)
            );
            await RefreshImage.ScaleTo(1, 125);

            for (int i = 1; i < 7; i++)
            {
                if (RefreshImage.Rotation >= 360f) RefreshImage.Rotation = 0;
                //await RefreshImage.RotateTo(i * (360 / 6), 200, Easing.Linear);
                RefreshImage.RotateTo(i * (360 / 6), 200, Easing.Linear);
            }
        }

        async void OnPowerTapped(object sender, EventArgs e)
        {
            await Task.WhenAny<bool>
            (
                PowerImage.ScaleTo(0.25, 125)
            );
            await PowerImage.ScaleTo(1, 125);
        }

        async void OnMenuSetTapped(object sender, EventArgs e)
        {
            await Task.WhenAny<bool>
            (
                MenuSet.ScaleTo(0.25, 125)
            );
            await MenuSet.ScaleTo(1, 125);
        }

        async void OnMenuInfoTapped(object sender, EventArgs e)
        {
            await Task.WhenAny<bool>
            (
                MenuInfo.ScaleTo(0.25, 125)
            );
            await MenuInfo.ScaleTo(1, 125);
        }


    }
}
