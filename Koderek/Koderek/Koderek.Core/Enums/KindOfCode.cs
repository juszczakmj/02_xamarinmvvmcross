﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Koderek.Core.Enums
{
    enum KindOfCode
    {
        NoSelectedKindOfCode=0, //Nie wybrano żadnego kodu
        CaesarASCII = 1,        //Caesar oparty na tablicy ASCII
        CaesarOwnTable =2       //Caesar oparty na wlasnej tablicy dopuszczalnych kodow, w tym znaki polskie
    }
}
