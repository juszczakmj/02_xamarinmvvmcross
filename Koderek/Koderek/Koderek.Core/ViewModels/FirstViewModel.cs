﻿using System.Threading.Tasks;
using System.Windows.Input;
using Java.Lang;
using Koderek.Core.Enums;
using Koderek.Core.IServices;
using Koderek.Core.LogModels;
using MvvmCross.Core.Navigation;
using MvvmCross.Core.ViewModels;
using Xamarin.Essentials;
using Xamarin.Forms;
using NoCodes = Koderek.Core.IServices.NoCodes;
using Process = Android.OS.Process;
using String = System.String;

namespace Koderek.Core.ViewModels
{
    public class FirstViewModel : MvxViewModel
    {
        readonly IMvxNavigationService navigationService;

        public FirstViewModel(IMvxNavigationService navigationService)
        {
            this.navigationService = navigationService;
        }

        private bool isHideYourWord = true;

        public bool IsHideYourWord
        {
            get { return isHideYourWord; }
            set { SetProperty(ref isHideYourWord, value); }
        }

        private bool isSwitched = true;

        public bool IsSwitched
        {
            get { return isSwitched; }
            set
            {
                if (SetProperty(ref isSwitched, value))
                {
                    RaisePropertyChanged(() => CurrActivity);
                    RaisePropertyChanged(() => InLabel);
                    RaisePropertyChanged(() => OutLabel);
                    RaisePropertyChanged(() => YourWordCrypted);
                }
            }
        }

        private string yourWord = string.Empty;

        public string YourWord
        {
            get { return yourWord; }
            set
            {
                if (SetProperty(ref yourWord, value))
                {
                    RaisePropertyChanged(()=>YourWordCrypted);
                }

            }
        }


        public string YourWordCrypted => RunCodeOn();

        public string CurrActivity => DescribeActivity(isSwitched);

        public string InLabel => DescribeInLabel(isSwitched);

        public string OutLabel => DescribeOutLabel(isSwitched);

        private string DescribeActivity(bool isSwitched)
        {
            if (isSwitched)
            {
                return "Szyfrowanie";
            }
            else
            {
                return "Deszyfrowanie";
            }
            
        }

        private string DescribeInLabel(bool isSwitched)
        {
            if (isSwitched)
            {
                return "Tekst do zaszyfrowania";
            }
            else
            {
                return "Tekst do odszyfrowania";
            }

        }

        private string DescribeOutLabel(bool isSwitched)
        {
            if (isSwitched)
            {
                return "Tekst zaszyfrowany";
            }
            else
            {
                return "Tekst odszyfrowany";
            }

        }

        ////public ICommand ShowSettingsPageCommand => new MvxAsyncCommand(() => navigationService.Navigate<SettingsViewModel>());
        public ICommand ShowSettingsPageCommand => new MvxAsyncCommand(() => RunShowSettingsPageCommand());
        //public ICommand ShowSettingsPageCommand => new MvxAsyncCommand(RunShowSettingsPageCommand);

        public ICommand ShowHelpPageCommand => new MvxAsyncCommand(() => RunShowHelpPageCommand());
        //public ICommand ShowHelpPageCommand => new MvxAsyncCommand(RunShowHelpPageCommand);

        public ICommand OnChangeClicked => new Command(()=>RunChangeClicked());

        public ICommand OnEyeClicked => new Command(()=>RunOnEyeClicked());

        public ICommand OnRefreshClicked => new Command(()=> RunOnRefreshClicked());

        public ICommand OnPowerClicked => new Command(()=> RunOnPowerClicked());

        public ICommand OnCopyToClipboardClicked => new Command((() => RunOnCopyToClipboardClicked()));

        public ICommand OnCopyFromClipboardClicked => new Command((() => RunOnCopyFromClipboardClicked()));

        public ICommand OnShareClicked => new Command((() => RunOnShareClicked()));

        public ICommand StopMyApplication => new Command(()=> RunStopMyApplication());

        public void RunStopMyApplication()
        {
            System.Environment.Exit(0);
        }

        public async Task RunShowHelpPageCommand()
        {
            await navigationService.Navigate<HelpViewModel>();
        }

        public async Task RunShowSettingsPageCommand()
        {
            await navigationService.Navigate<SettingsViewModel>();
        }

        //udostępnianie wygenerowanego szyfru
        public async Task RunOnShareClicked()
        {
            //Task.Run uruchamia oddzielny wątek
            await Task.Run(() => Share.RequestAsync(new ShareTextRequest
            {
                Text = YourWordCrypted,
                Title = "Wygenerowano w Koderek: "
            }));
        }

        //wklejanie tekstu ze schowka
        public async void RunOnCopyFromClipboardClicked()
        {
           YourWord = await Clipboard.GetTextAsync();
           RaisePropertyChanged(()=>YourWordCrypted);
        }

        //kopiowanie zakodowanego tekstu do schowka
        public async void RunOnCopyToClipboardClicked()
        {
            await Clipboard.SetTextAsync(YourWordCrypted);
        }

        //public async void RunOnRefreshClicked()
        public void RunOnRefreshClicked()
        {
            RaisePropertyChanged(() => YourWordCrypted);
        }

        //public async void RunOnPowerClicked()
        public void RunOnPowerClicked()
        {
            YourWord=String.Empty;
        }

        //public async void RunOnEyeClicked()
        public void RunOnEyeClicked()
        {
            IsHideYourWord =!IsHideYourWord;
        }

        //public async void RunChangeClicked()
        public void RunChangeClicked()
        {
            YourWord = YourWordCrypted;
        }

        /// <summary>
        /// Tutaj wywołania odpowiednich algorytmow szyfrujących.
        /// Kazda z metod działa na klasie generycznej
        /// </summary>
        public string RunCodeOn()
        {
            string result=String.Empty;

            switch (ConfigureApp.CurrentKindOfCode)
            {
                case (int)KindOfCode.NoSelectedKindOfCode:
                    result = NoCodeSolve();
                    break;
                case (int)KindOfCode.CaesarASCII:
                    result = CaesarSolve();
                    break;
                case (int)KindOfCode.CaesarOwnTable:
                    result = CaesarSolve2();
                    break;
            }

            //return CaesarSolve();
            return result;
        }

        public string CaesarSolve()
        {
            Encryption enc = new Encryption();
            Caesar cas = new Caesar();

            if (IsSwitched)
            {
                cas.InText = YourWord;
                enc.EnCode<Caesar>(cas);
                return cas.OutText;
            }
            else
            {
                cas.OutText = YourWord;
                enc.DeCode<Caesar>(cas);
                return cas.InText;
            }
        }

        public string CaesarSolve2()
        {
            Encryption enc = new Encryption();
            Caesar2 cas = new Caesar2();

            if (IsSwitched)
            {
                cas.InText = YourWord;
                enc.EnCode<Caesar2>(cas);
                return cas.OutText;
            }
            else
            {
                cas.OutText = YourWord;
                enc.DeCode<Caesar2>(cas);
                return cas.InText;
            }
        }

        public string NoCodeSolve()
        {
            Encryption enc = new Encryption();
            NoCodes cas = new NoCodes();

            if (IsSwitched)
            {
                cas.InText = YourWord;
                enc.EnCode<NoCodes>(cas);
                return cas.OutText;
            }
            else
            {
                cas.OutText = YourWord;
                enc.DeCode<NoCodes>(cas);
                return cas.InText;
            }
        }

    }
}
