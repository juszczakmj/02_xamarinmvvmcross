﻿using System;
using System.Windows.Input;
using Koderek.Core.LogModels;
using MvvmCross.Core.ViewModels;
using Xamarin.Forms;

namespace Koderek.Core.ViewModels
{
    public class HelpViewModel : MvxViewModel
    {
        public ICommand OpenZGWebCommand => new Command(() => Device.OpenUri(new Uri("http://zakodujgarwolin.pl/")));
    }
}