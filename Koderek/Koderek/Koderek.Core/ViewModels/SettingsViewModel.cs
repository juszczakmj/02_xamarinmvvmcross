﻿using Koderek.Core.Enums;
using Koderek.Core.LogModels;
using MvvmCross.Core.ViewModels;

namespace Koderek.Core.ViewModels
{
    public class SettingsViewModel : MvxViewModel
    {
        private int currentTranslationValue=ConfigureApp.CaesarTranslation;

        public int CurrentTranslationValue
        {
            get { return currentTranslationValue; }
            set
            {
                if (SetProperty(ref currentTranslationValue, value))
                {
                    ConfigureApp.CaesarTranslation = currentTranslationValue;
                }
            }
        }

        private int currentTranslationValue2 = ConfigureApp.CaesarTranslation;

        public int CurrentTranslationValue2
        {
            get { return currentTranslationValue2; }
            set
            {
                if (SetProperty(ref currentTranslationValue2, value))
                {
                    ConfigureApp.CaesarTranslation = currentTranslationValue2;
                }
            }
        }

        public int MinTranslation => ConfigureApp.CaesarMinTranslation;

        public int MaxTranslation => ConfigureApp.CaesarMaxTranslation;

        /// <summary>
        /// USTAWIENIA SWITCH ON/OFF
        /// </summary>

        private static bool isSwitchedCaesar1; //=true;

        public bool IsSwitchedCaesar1
        {
            get { return isSwitchedCaesar1;}
            set
            {
                if (SetProperty(ref isSwitchedCaesar1, value))
                {
                    SetAllOtherSwitchers((int)KindOfCode.CaesarASCII, isSwitchedCaesar1);
                }
            }
        }

        private static bool isSwitchedCaesar2; //= false;

        public bool IsSwitchedCaesar2
        {
            get { return isSwitchedCaesar2; }
            set
            {
                if (SetProperty(ref isSwitchedCaesar2, value))
                {
                    SetAllOtherSwitchers((int)KindOfCode.CaesarOwnTable, isSwitchedCaesar2);
                }
            }
        }


        /// <summary>
        /// USTAWIENIA TŁA ACTIVE/NOACTIVE
        /// </summary>
        private static string caesar1Bkgr = ConfigureApp.NoActiveFrameBackground;

        public string Caesar1Bkgr
        {
            get { return caesar1Bkgr; }
            set { SetProperty(ref caesar1Bkgr, value); }
        }

        private static string caesar2Bkgr = ConfigureApp.NoActiveFrameBackground;

        public string Caesar2Bkgr
        {
            get { return caesar2Bkgr; }
            set { SetProperty(ref caesar2Bkgr, value); }
        }


        /// <summary>
        /// wylacza wszytkie switche poza tym whichon o ile wartośćdrugiego parametru==true
        /// </summary>
        /// <param name="whichon"></param>
        private void SetAllOtherSwitchers(int whichon, bool truesetted)
        {
            if (truesetted)     //jesli dany switch ustawiono na tru to wtedy zmien pozostale na false
            {
                ConfigureApp.CurrentKindOfCode = whichon;


                switch (whichon)
                {
                    case (int)KindOfCode.CaesarASCII:
                        IsSwitchedCaesar2 = false;
                        Caesar1Bkgr = ConfigureApp.ActiveFrameBackground;
                        Caesar2Bkgr = ConfigureApp.NoActiveFrameBackground;
                        RaisePropertyChanged(() => IsSwitchedCaesar2);
                        RaisePropertyChanged(() => Caesar2Bkgr);
                        break;
                    case (int)KindOfCode.CaesarOwnTable:
                        IsSwitchedCaesar1 = false;
                        Caesar2Bkgr = ConfigureApp.ActiveFrameBackground;
                        Caesar1Bkgr = ConfigureApp.NoActiveFrameBackground;
                        RaisePropertyChanged(() => IsSwitchedCaesar1);
                        RaisePropertyChanged(() => Caesar1Bkgr);
                        break;
                }
                
            }
            
        }


    }
}
